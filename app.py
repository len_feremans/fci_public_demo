###based on tutor https://code.tutsplus.com/tutorials/creating-a-web-app-from-scratch-using-python-flask-and-mysql--cms-22972
import datetime
import os.path
import subprocess
import pandas as pd
import io

from flask import Flask
from flask.templating import render_template
from flask import request, session, jsonify

from python_wrapper import runFCI_seq
import save_to_html
### INITIALIZE SINGLETON SERVICES ###
app = Flask('Tutorial ')
app.secret_key = '*^*(*&)(*)(*afafafaSDD47j\3yX R~X@H!jmM]Lwf/,?KT'
app_data = {}
app_data['app_name'] ='Cohesive Pattern Mining Demo'

#global app state: should be session...
current_patterns = []
current_parameters = {}
current_sequence_html = ''

### VIEW ###
@app.route("/")
def main():
    global current_parameters
    return render_template('index.html', app_data=app_data, parameters=current_parameters)

@app.route('/load_data/<string:key>',methods=['GET'])
def load_data(key): 
    content = ''
    filename = "./data/{}.txt".format(key)
    if os.path.exists(filename):
        #f = open(filename, mode='r', encoding='utf-8')
        f= io.open(filename, encoding='utf-8')
        content = f.readlines()
        content = ''.join(content)
    else:
        content = ''
    return jsonify(content)

@app.route('/mine',methods=['POST','GET'])
def mine(): 
    global current_parameters
    global current_patterns
    global current_sequence_html
    if request.method == 'GET':
        return render_template('patterns.html', app_data=app_data, patterns=current_patterns, sequence_html=current_sequence_html, parameters=current_parameters)
    #e.g. 'ImmutableMultiDict([('{"txt_key":"trump","minsup":"4","maxsize":"4","isItemsets":true,"isSp":false,"isEpi":false,"isRul":false,
    #                            "minOrSp":"","minOrEpi":"","conf":""}', '')])
    d = request.form
    print(d)
    try:
        txt_key = d.get("txt_key")
        minsup = int(d.get("minsup"))
        maxsize = int(d.get("maxsize"))
        mincoh = float(d.get("mincoh"))
        isItemset = bool(d.get("isItemset"))
        isSp = bool(d.get("isSp"))
        isEpi = bool(d.get("isEpi"))
        isRul = bool(d.get("isRul"))
        minOrSp = float(d.get("minOrSp")) if isSp else None
        minOrEpi = float(d.get("minOrEpi")) if isEpi else None
        conf = float(d.get("conf")) if isRul else None
        split = int(d.get("split")) if d.get("split") else None
        current_parameters = {'txt_key': txt_key, 'minsup':minsup, 'maxsize': maxsize, 'mincoh': mincoh, 
                          'isItemset': isItemset, 'isSp': isSp, 'isEpi': isEpi, 'isRul': isRul, 
                          'minOrSp': minOrSp,'minOrEpi': minOrEpi, 'conf': conf, 'split':split}
        filename = "./data/{}.txt".format(txt_key)
        ofilename_patterns='./temp/patterns-{}.txt'.format(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d-%H:%M"))
        #filename='./temp/data-{}.txt'.format(datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%d-%H:%M"))
        #f = open(filename, txt, encoding='utf-8')
        #f.write(txt)
        #f.close()
    except Exception as e:
        message = 'Invalid arguments.\nError:' + str(e)
        print(message)
        return render_template('index.html', app_data=app_data, parameters=current_parameters, message=message)
    try:
        (output,error,returncode) = runFCI_seq(labels_file=None, sequence_file=filename, min_sup=minsup, max_size=maxsize, min_coh=mincoh, 
               seq=minOrSp, epi=minOrEpi, rule=conf, sparse=False, split=split, preprocess=True,
               outputfile=ofilename_patterns, timeout=60)
        print("output:" + output)
        print("error:" + error)
        print("return code:" + str(returncode))
        console_output = output + '\n' + error
    except subprocess.TimeoutExpired as e:
        #console_output = e.stdout.decode(sys.stdout.encoding).strip() + '\n' + e.stderr.decode(sys.stdout.encoding).strip()
        message = 'For security reasons, time-out for pattern mining is limited to 60 seconds.'
        print(message)
        return render_template('index.html', app_data=app_data, parameters=current_parameters, message=message)
    if console_output.find('Exception') != -1 or returncode !=0:
        message = 'An error occurred during pattern mining.\nConsole output:\n' + console_output
        print(message)
        return render_template('index.html', app_data=app_data, parameters=current_parameters, message=message)
    #prepare output
    try:
        (patterns,sequence_html) = patterns2view(filename, ofilename_patterns, minsup)
        #save global state
        current_patterns= patterns
        current_sequence_html = sequence_html
    except Exception as e:
        message = 'An error occurred during rendering patterns.\nError:' + str(e) + '\nConsole output:\n' + console_output
        return render_template('index.html', app_data=app_data, parameters=current_parameters, message=message)
    #render
    return render_template('patterns.html', app_data=app_data, patterns=patterns, sequence_html=current_sequence_html, parameters=current_patterns)

def patterns2view(filename_sequence_orig, filename_patterns, minsup):
    df = pd.read_csv(filename_patterns,sep=';',dtype = str, index_col=False)
    df = df.fillna('')
    print(df.head(3))
    #format for output
    if 'sequential pattern' in  df.columns:
        df['sequential pattern'] = df['sequential pattern'].apply(lambda x: x[1:-1] if x!='' else '')
    if 'episode' in  df.columns:
        df['episode'] = df['episode'].apply(lambda x: x[1:-1] if x != '' else '')
    df['cohesion'] = df['cohesion'].apply(lambda x: '{0:0.3f}'.format(float(x)) if x !='' else '')
    if 'occurence ratio'  in  df.columns:
        df['occurence ratio'] = df['occurence ratio'].apply(lambda x: '{0:0.3f}'.format(float(x)) if x !='' else '')
    print(df.head(3))
    patterns = [list(df.columns.values)] + df.values.tolist();
    if len(patterns) > 1 :
        print(patterns[0:2])
    #get itemsets, also for different pattern types
    itemsets = []
    if 'itemset' in df.columns:
        itemsets= df['itemset'].values.tolist()
    elif 'sequential pattern' in df.columns:
        itemsets= df['sequential pattern'].values.tolist()
        itemsets = [itemset.replace(',',' ') for itemset in itemsets]
    elif 'episode' in df.columns:
        itemsets= df['episode'].values.tolist()
        itemsets = [itemset.replace('->',' ').replace(',',' ') for itemset in itemsets]
    elif 'rule' in df.columns:
        itemsets= df['rule'].values.tolist()
        itemsets = [itemset.replace('->',' ').replace(',',' ') for itemset in itemsets]
    sequence_html = save_to_html.sequence2html(filename_sequence_orig, itemsets, minsup)
    return (patterns, sequence_html)

### RUN DEV SERVER ###
if __name__ == "__main__":
    app.run()
    #TODO: change
    #app.run(host='huppeldepup', port=5000)
