# README #

Implementation of online web-application that serves as demo for _Efficiently Mining Cohesion-based Patterns and Rules in Event Sequences_, 
by _Len Feremans_, data scientist at the University of Antwerp (Belgium) within the Adrem data labs 
research group. Paper authors are Boris Cule, Len Feremans, and Bart Goethals- University of Antwerp, Belgium.

Accepted to Data Mining and Knowledge Discovery on 2019-02-11

At this time an online version is available at [Online Version](http://houweel.uantwerpen.be:5000/).

![Screenshot1](https://bytebucket.org/len_feremans/fci_public_demo/raw/006183a3da09fa3b9e70b04bca9f46cae6c5d770/patterns.png)
![Screenshot2](https://bytebucket.org/len_feremans/fci_public_demo/raw/006183a3da09fa3b9e70b04bca9f46cae6c5d770/examples.png)


### Abstract ###
> Discovering patterns in long event sequences is an important data mining task. Traditionally, research focused on frequency-based quality measures that allow algorithms to use the anti-monotonicity property to prune the search space and efficiently discover the most frequent patterns. In this work, we step away from such measures, and evaluate patterns using cohesion — a measure of how close to each other the items making up the pattern appear in the sequence on average. We tackle the fact that cohesion is not an anti-monotonic measure by developing an upper bound on cohesion in order to prune the search space. By doing so, we are able to efficiently unearth rare, but strongly cohesive, patterns that existing methods often fail to discover. Furthermore, having found the occurrences of cohesive itemsets in the input sequence, we use them to discover the most dominant sequential patterns and partially ordered episodes, without going through the computationally expensive candidate generation procedures typically associated with sequence and episode mining. Experiments show that our method efficiently discovers important patterns that existing state-of-the-art methods fail to discover.

### What is this repository for? ###
Web-application demo for mining patterns, ranked using cohesion, efficiently. 
Command line version of tool, and algorithm, is available at <https://bitbucket.org/len_feremans/fci_public/>

You can use this web-application to mine _cohesive_ itemsets, and also _dominant sequential patterns_, _dominant episodes_ and _association rules_ in timestamped 
or continuous long event sequence of events, online. Itemsets are ranked using _cohesion_, where cohesion is defined as the average (for each item occurrence 
_t_ in the sequence_) of _minimal window_ lengths, where the minimal window is the minimal subsequence at _t_ that contains all items of the itemset.

### Quick start ###
This web-application implementation is written in _Python_ (3) and uses Flask, Pandas, Bootstrap, JQuery and Material Design Lite.
Compiled version of FCI-seq-1.0.0-jar included as Jar in this repo.

To rune the web-app use:
```
python app.py
```

### Who do I talk to? ###
E-mail Len Feremans (gmail) for more information.

### Licence ###
Copyright (c) [2019] [Universiteit Antwerpen - Len Feremans]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.