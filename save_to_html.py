# -*- coding: utf-8 -*-
'''
Prototype code
Finding Top-k Quantile-based Cohesive Patterns in Sequences

Goal: 
Show original text fragment in html, with color-coding of items/stopwords and infrequent items
Show top-k patterns hightlighted in HTML 

Author: Len Feremans
'''
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
from collections import Counter
import re
'''
classes used:
.stopword {
   color: #C4C4C4;
}
.lowsupport {
   color: #A4A4A4;
}
.focus{
   background-color: #FDE4AE;
}
'''
def sequence2html(file_orig, itemsets, minsup, only_matches=False):
    #e.g. Call me Ishmael. Some years ago—never mind how long precisely—having little or no money in my purse, and nothing particular to interest me on shore,
    # I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen and regulating the circulation. 
    f = open(file_orig, encoding='utf-8')
    lines_orig = f.readlines()
    f.close()
    
    #tokenize simple
    lines_orig = [tokenize(line) for line in lines_orig] 
    
    #make dictionary, again in python
    cnt = Counter()
    for line in lines_orig:
        for token in line:
            cnt[stem(token)] +=1
        
    #make index of itemsets
    items = {}
    for itemset in itemsets:
        for item in itemset.split():
            items[item] = True
            
    #save sequence formatted
    punctuation = ['.', '!','?',',','-','"',"'",'“','”','@','#']
    html_content = ''
    for line in lines_orig:
        match = False
        html_line = ''
        for token in line:
            if token.strip() == '':
                continue
            elif token in punctuation:
                html_line += token + ' '
            elif stem(token) in items:
                html_line += '<span class="focus">{}</span>'.format(token) + ' '
                match = True
            elif is_stopword_rainbow(token):
                html_line += '<span class="stopword">{}</span>'.format(token) + ' '
            elif cnt[stem(token)] < minsup:
                html_line += '<span class="lowsupport">{}</span>'.format(token) + ' '
            else:
                html_line += token + ' '
        if only_matches:
            if match:
                html_content += '<p>' + html_line + '</p>\n'
        else:
            html_content += '<p>' + html_line + '</p>\n'
    return html_content

def tokenize(line):
    #see https://stackoverflow.com/questions/21023901/how-to-split-at-all-special-characters-with-re-split
    tokens = re.split(r"[^a-zA-Z0-9]", line)
    tokens = [token for token in tokens if token.strip() != '']
    #add seperators as tokens
    new_tokens = []
    for token in tokens:
        if line.startswith(token):
            new_tokens.append(token)
            line = line[len(token):]
        else:
            idx = line.index(token)
            new_tokens.append(line[0:idx])
            new_tokens.append(token)
            line = line[idx + len(token):]
    new_tokens = [token.strip() for token in new_tokens if token.strip() != '']
    return new_tokens
        
stopwords = None
def is_stopword_rainbow(word):
    global stopwords
    if stopwords == None:
        f = open('./stop_words_rainbow.txt')
        lines = f.readlines()
        f.close()
        stopwords = {}
        for line in lines:
            stopwords[line.strip()] = True
    return word in stopwords

def stem(token):
    stemmed = token.lower().strip()
    stemmed = stemmer.stem(stemmed)
    return stemmed


def clamp_rgb(r, g, b): #see also https://stackoverflow.com/questions/141855/programmatically-lighten-a-color
    return min(255, int(r)), min(255, int(g)), min(255, int(b))

tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]
             
  
# Rescale to values between 0 and 1 
for i in range(len(tableau20)):  
    r, g, b = tableau20[i]  
    tableau20[i] = (r / 255., g / 255., b / 255.)
    
    
def test_sequence2html():
    style = '''.stopword {
   color: #E4E4E4;
}
.lowsupport {
   color: #ABABAB;
}
.focus{
   background-color: #FDE4AE;
}'''
    file = './data/species.txt'
    ofile = './data/species.html'
    html = '<html><head><style>' + style + '</style></head><body>' + sequence2html(file, ['mobi dick','captain ahab'], 3) + '</body></html>'
    f = open(ofile, 'w', encoding='utf-8')
    f.write(html)
    f.close()
    print("saved " + ofile)
  
def test_tokenize():
    line = '''Today @FLOTUS, Melania and I, were honored to welcome. Prime Minister @TurnbullMalcolm and Mrs. Turnbull.'''
    print(tokenize(line))
    print(line)
    print(' '.join(tokenize(line)))
    
def test_stem():
    print(stem('News'))
    
#test_tokenize()
#test_sequence2html() 
test_stem()