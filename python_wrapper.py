import sys
import os.path
import subprocess
PATH_FCI_SEQ = './lib/FCI-seq-1.0.0-jar-with-dependencies.jar'

if os.path.isfile(PATH_FCI_SEQ):
    print("Found dependencies")
else:
    print("ERROR: Please change python_wrapper.py and refer to existing directories.  PATH_FCI_SEQ=" + os.path.abspath(PATH_FCI_SEQ))
    
def runFCI_seq(labels_file, sequence_file, min_sup=1, max_size=5, min_coh=0.5, seq=None, epi=None, rule=None, 
               sparse=False, split=None, preprocess=False, outputfile=None, timeout=60):
    command = ["java", "-Djava.awt.headless=true", "-jar", os.path.abspath(PATH_FCI_SEQ), "-s", str(min_sup), "-m", str(max_size), "-c", str(min_coh), "-i", str(os.path.abspath(sequence_file)), "-v"]
    if labels_file:
        command =  command + ["-l", os.path.abspath(labels_file)]
    if seq:
        command =  command + ["-seq", str(seq)]
    if epi:
        command =  command + ["-epi", str(epi)]
    if rule:
        command =  command + ["-rul", str(rule)]
    if sparse:
        command =  command + ["-S"]
    if split:
        command =  command + ["-split", str(split)]
    if preprocess:
        command =  command + ["-P"]
    if outputfile:
        command = command + ["-o", os.path.abspath(outputfile)]
    print(" ".join(command))
    #subprocess.check_output(command, timeout=timeout).decode(sys.stdout.encoding).strip() #default: 1 minute time out
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE,)
    output, error = p.communicate(timeout=timeout)
    output = output.decode(sys.stdout.encoding).strip()
    error = error.decode(sys.stdout.encoding).strip()
    return (output,error,p.returncode)